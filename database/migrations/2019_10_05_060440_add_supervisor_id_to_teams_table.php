<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupervisorIdToTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->unsignedBigInteger('supervisor_id')->nullable()->after('id');
            $table->unsignedBigInteger('schedule_id')->nullable()->after('supervisor_id');
            // Foreign keys
            $table->foreign('supervisor_id')->references('id')->on('employees')->onDelete('set null');
            $table->foreign('schedule_id')->references('id')->on('schedules')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign(['supervisor_id']);
            $table->dropForeign(['schedule_id']);
            $table->dropColumn(['supervisor_id']);
            $table->dropColumn(['schedule_id']);
        });
    }
}
