<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedDetailsTable extends Migration
{
    /**
     * Run the migrations.
     * 'operator_id', 'supervisor_id', 'machine_id', 'urdimbre', 'meters', 'final_meters_prod', 'eficienty', 'operation_time', 'out_for_lunch'
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('operator_id')->nullable();
            $table->unsignedBigInteger('supervisor_id')->nullable();
            $table->unsignedBigInteger('machine_id')->nullable();
            $table->double('urdimbre');
            $table->double('meters');
            $table->double('final_meters_prod');
            $table->double('eficienty');
            $table->double('operation_time');
            $table->unsignedTinyInteger('out_for_lunch');	
            $table->timestamps();
            $table->softDeletes();

            // Foreign keys
            $table->foreign('operator_id')->references('id')->on('employees')->onDelete('set null');
            $table->foreign('supervisor_id')->references('id')->on('employees')->onDelete('set null');
            $table->foreign('machine_id')->references('id')->on('machines')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_details');
    }
}
