<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'Super admin',
                'permissions' => '{"user":["viewAll","view","create","update","delete"],"employee":["viewAll","view","create","update","delete"],"schedule":["viewAll","view","create","update","delete"],"machine":["viewAll","view","create","update","delete"],"goal":["viewAll","view","create","update","delete"],"team":["viewAll","view","create","update","delete"]}',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'name' => 'Admin',
                'permissions' => '{"user":["viewAll","view","create","update","delete"],"employee":["viewAll","view","create","update","delete"],"schedule":["viewAll","view","create","update","delete"],"machine":["viewAll","view","create","update","delete"],"goal":["viewAll","view","create","update","delete"],"team":["viewAll","view","create","update","delete"]}',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'name' => 'Supervisor',
                'permissions' => '{"user":["viewAll","view","create","update","delete"],"employee":["viewAll","view","create","update","delete"],"schedule":["viewAll","view","create","update","delete"],"machine":["viewAll","view","create","update","delete"],"goal":["viewAll","view","create","update","delete"],"team":["viewAll","view","create","update","delete"]}',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    }
}
