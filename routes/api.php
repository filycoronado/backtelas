<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'users'], function() {
    Route::get('unauthorized', ['as' => 'unauthorized', 'uses' => 'UserController@unauthorized']);
    // custom functions requests here
     Route::post('doLogin', 'UserController@doLogin');
     Route::post('addUser', 'UserController@addUser');
     Route::post('updateUser', 'UserController@updateUser');
     Route::get('getAll', 'UserController@index')->middleware(['auth:api', 'permissions:viewAll,user']);
     Route::get('getUsersDto', 'UserController@getUsersDto');
     Route::get('/{id}', 'UserController@show');
     Route::get('/{id}', 'UserController@show');
     Route::post('updateUser', 'UserController@update');
});

Route::group(['prefix' => 'employee'], function() {
    // custom functions requests here
    Route::get('getAll', 'EmployeeController@getAll');
    Route::get('getPages', 'EmployeeController@DoEmployees');
    Route::get('availables', 'EmployeeController@availables');
    Route::get('getById/{id}', 'EmployeeController@getById');
    Route::get('schedule/{id}', 'EmployeeController@getSchedule');
    Route::get('getSuperVisorTeam', 'EmployeeController@getSuperVisorTeam');
    Route::post('addEmployee', 'EmployeeController@addEmployee')->middleware('permissions:create,employee');
    Route::post('uppdateEmployee', 'EmployeeController@uppdateEmployee');
    Route::get('getMachines', 'EmployeeController@getMachines');
});

Route::group(['prefix' => 'roles'], function() {
    Route::get('getAll', 'RolesController@getAll');
    Route::get('getAllPermissions', 'RolesController@getAllPermissions');
    Route::get('getRolePermissions', 'RolesController@getRolePermissions');
    Route::post('updateRolePermissions', 'RolesController@updateRolePermissions');
});

Route::group(['prefix' => 'teams'], function() {
    Route::get('getAll', 'TeamController@getAll');
    Route::get('getById/{id}', 'TeamController@getById');
    Route::get('getTeamEmployees', 'TeamController@teamEmployees');
    Route::post('addTeam', 'TeamController@addTeam');
    Route::post('updateTeam', 'TeamController@updateTeam');
});

Route::group(['prefix' => 'goals'], function() {
    Route::post('addGoal', 'GoalController@addGoal');
    Route::get('getAll', 'GoalController@getAll');
    Route::get('getById', 'GoalController@getById');
    Route::get('getByTeam/{id}', 'GoalController@getByTeam');
});

Route::group(['prefix' => 'schedules'], function() {
    Route::get('getAll', 'ScheduleController@getAll');
    Route::post('addSchedule', 'ScheduleController@createSchedule');
    Route::get('getById/{id}', 'ScheduleController@findById');
    Route::post('uppdateSchedule', 'ScheduleController@uppdateSchedule');
    Route::get('getByEmployee/{id}', 'ScheduleController@getByEmployee');
    Route::post('saveEmployeeSchedule', 'ScheduleController@saveEmployeeSchedule');

});

Route::group(['prefix' => 'machines'], function() {
    Route::get('getAll', 'MachineController@getAll');
    Route::post('addMachine', 'MachineController@createMachine');
    Route::get('getById/{id}', 'MachineController@findById');
    Route::post('updateMachine', 'MachineController@updateMachine');
    Route::post('deleteMachine', 'MachineController@deleteMachine');
});
Route::group(['prefix' => 'teamGoal'], function() {
    Route::get('getAll', 'TeamGoalController@getAll');
    Route::get('getById', 'TeamGoalController@getByid');
    Route::post('addTeamGoal', 'TeamGoalController@addTeamGoal');
});

Route::group(['prefix' => 'assigned_details'], function() {
    Route::get('getAssignedDetail', 'AssignedDetailController@getAssignedDetail');
});


Route::group(['prefix' => 'teamsEmployees'], function() {
    Route::post('syncTeamsEmployees', 'TeamController@syncTeamsEmployees');
    Route::get('getTeamEmployees/{id}', 'TeamController@teamEmployees');
});

Route::group(['prefix' => 'reports'], function() {
    Route::get('generateReport', 'ReportController@generateReport');
    Route::get('getChartsData', 'ReportController@getChartsData');
    Route::get('exportReport', 'ReportController@exportReport');
    Route::get('importExportView', 'ReportController@importExportView');
});

Route::group(['prefix' => 'employeeMachines'], function () {
    Route::post('add', 'EmployeeMachine@add');
    Route::get('getAll', 'EmployeeMachine@getAll');
    Route::get('getByEmployee/{id}', 'EmployeeMachine@getByEmployee');
    Route::put('update', 'EmployeeMachine@update');
    Route::delete('delete', 'EmployeeMachine@delete');
});
