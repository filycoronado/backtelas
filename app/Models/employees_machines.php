<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class employees_machines extends Model
{
    //
    use SoftDeletes;

    protected $fillable = [
        'employee_id', 'machine_id', 'extra_machine', 'workshift', 'active'
    ];

    public function getEmployee() {
        return $this->hasOne('App\Models\Employee', 'employee_id', 'id');
    }

    public function getMachine() {
        return $this->hasOne('App\Models\Machine');
    }
}
