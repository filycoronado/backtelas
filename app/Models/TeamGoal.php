<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class TeamGoal extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id', 'goal_id'
    ];

    public function getTeam() {
        return $this->hasOne('App\Models\Team');
    }

    public function getGoal() {
        return $this->hasOne('App\Models\Goal', 'id', 'goal_id');
    }
}
