<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'address'
    ];

    public function getuser()
    {
        return $this->hasOne('App\Models\User');
    }

    public function teams()
    {
     return $this->belongsToMany('App\Models\Team', 'teams_employees', 'team_id', 'employee_id');
    }

    public function machines()
    {
     return $this->belongsToMany('App\Models\Machine', 'employees_machines', 'employee_id', 'machine_id');
    }

    public function supervisorTeam()
    {
     return $this->hasOne('App\Models\Team', 'supervisor_id', 'id');
    }

    public function schedule()
    {
     return $this->belongsToMany('App\Models\Schedule', 'employees_schedules', 'employee_id', 'schedule_id')->whereNull('employees_schedules.deleted_at')->withPivot('id');
    }

    public static function scopeListado($query, $start, $perPage, $search="")
    {
        $total = $query->count(); // count total rows
        if( $search != "")
        {
            $query->where('employees.first_name', 'like', '%'. $search .'%')->orWhere('employees.last_name', 'like', '%'. $search .'%');
        }
        $count = $query->count(); // count total rows

        $page=0;
        if( $perPage != 0 ){
            $query = $query->skip(intval($start-1))->take($perPage); //$query->skip(($start-1)*$perPage)->take($perPage);
            $page = (($start-1)+$perPage)/$perPage; //($start-1)+$perPage/$perPage;
        }

        $pages = round($count/$perPage);

        $query = $query->select('employees.id',
                                'employees.first_name',
                                'employees.last_name',
                                'employees.email',
                                'employees.address',
                                'employees.phone')
                                ->get();

        $meta['recordsTotal'] = $total;
        $meta['recordsFiltered'] = $count;
        $meta['start'] = $start;
        $meta['page'] = round($page);
        $meta['perpage'] = $perPage;
        $meta['pages'] = round($pages);
        $meta['length'] = $count;

        $results = [
            'meta' => $meta,
            'recordsTotal' => $total,
            'recordsFiltered' => $count,
            'list' => $query
        ];
        return $results;
    }
}
