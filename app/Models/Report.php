<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assigned_details';

    public static function scopeListado($query, $columns,  $start, $perPage, $search="",$order, $draw)
    {
        $total = $query->count(); // count total rows
        if( $search != "")
        {
            $query->where('clients.first_name', 'like', '%'. $search .'%')->orWhere('clients.last_name', 'like', '%'. $search .'%');
            /*$query = $query->where(function($query) use ($search){
                $query->where('checklist.name', 'like', '%'. $search .'%');
            });*/
        }
        $count = $query->count(); // count total rows

        if( $order ){
            $query = $query->orderBy($columns[intval($order['column'])], $order['dir']);
        }

        $page=0;
        if( $perPage != 0 ){
            $query = $query->skip(intval($start))->take($perPage); //$query->skip(($start-1)*$perPage)->take($perPage);
            $page = (($start-1)+$perPage)/$perPage; //($start-1)+$perPage/$perPage;
        }

        $pages = 0;
        if ($perPage != 0) {
            $pages = round($count/$perPage);
        }

        $query = $query->select('assigned_details.id',
                                'assigned_details.supervisor_id',
                                'assigned_details.operator_id',
                                'assigned_details.machine_id',
                                'assigned_details.urdimbre',
                                'assigned_details.meters',
                                'assigned_details.final_meters_prod',
                                'assigned_details.eficienty',
                                'assigned_details.operation_time',
                                'assigned_details.out_for_lunch',
                                'assigned_details.created_at',
                                'machines.trama as trama',
                                'machines.pics as pics',
                                //'employees_schedules.schedule_id as schedule_id',
                                'schedules.turn as turn',
                                //DB::raw('(SELECT CONCAT(employees.first_name," ",employees.last_name) FROM employees WHERE assigned_details.supervisor_id = employees.id) as supervisor'),
                                //DB::raw('(SELECT CONCAT(employees.first_name," ",employees.last_name) FROM employees WHERE assigned_details.operator_id = employees.id) as operator'),
                                //DB::raw('(SELECT schedules.turn FROM schedules INNER JOIN assigned_details ON employees_schedules.schedule_id = schedules.id INNER JOIN employees_schedules ON employees_schedules.schedule_id = schedules.id AND employees_schedules.employee_id = assigned_details.supervisor_id ) as turn'),
                                //(SELECT CONCAT(employees.first_name," ",employees.last_name) FROM employees WHERE assigned_details.supervisor_id = employees.id) as supervisor
                                //DB::raw('CONCAT(employees.first_name," ",employees.last_name) as operator')
                                //'supervisors.first_name as supervisor',
                                //'operators.first_name as operator',
                                DB::raw('CONCAT(supervisors.first_name, " ", supervisors.last_name) as supervisor'),
                                DB::raw('CONCAT(operators.first_name, " ", operators.last_name) as operator'),
                                )
                                ->leftjoin('employees AS supervisors', 'supervisors.id', '=', 'assigned_details.supervisor_id')
                                ->leftjoin('employees AS operators', 'operators.id', '=', 'assigned_details.operator_id')
                                ->leftjoin('machines', 'machines.id', '=', 'assigned_details.machine_id')
                                //->leftjoin('employees_schedules', 'employees_schedules.employee_id', '=', 'assigned_details.supervisor_id')
                                ->leftJoin('employees_schedules', function($join){
                                        $join->on('employees_schedules.employee_id', '=', 'assigned_details.supervisor_id')
                                        ->where('employees_schedules.deleted_at', '!=', 'NULL');
                                })
                                /* ->leftJoin(DB::raw('(SELECT schedule_id FROM employees_schedules WHERE employee_id = assigned_details.supervisor_id GROUP BY employee_id)
                                                    schedule'), function($join)
                                    {
                                        $join->on('schedule.id', '=', 'assigned_details.supervisor_id');
                                    }) */
                                ->join('schedules', 'employees_schedules.schedule_id', '=', 'schedules.id')
                                //->where('employees_schedules.deleted_at', '!=', 'NULL')
                                ->get();

        $meta['recordsTotal'] = $total;
        $meta['recordsFiltered'] = $count;
        $meta['draw'] = $draw;
        $meta['field'] = $order['column'];
        $meta['start'] = $start;
        $meta['page'] = round($page);
        $meta['perpage'] = $perPage;
        $meta['pages'] = round($pages);
        $meta['length'] = $count;

        $results = [
            'meta' => $meta,
            'recordsTotal' => $total,
            'recordsFiltered' => $count,
            'data' => $query
        ];
        return $results;
    }

    public static function scopeChartsData($query, $date_start = "", $date_end = "")
    {
        if ($date_start != "" && $date_end != "") {
            $query->whereBetween('assigned_details.created_at', [$date_start, $date_end]);
        }
        $query = $query->select('assigned_details.id',
                                'assigned_details.supervisor_id',
                                'assigned_details.operator_id',
                                'assigned_details.machine_id',
                                'assigned_details.urdimbre',
                                'assigned_details.meters',
                                'assigned_details.created_at',
                                'machines.name as machine_name',
                                DB::raw('CONCAT(supervisors.first_name, " ", supervisors.last_name) as supervisor'),
                                DB::raw('CONCAT(operators.first_name, " ", operators.last_name) as operator'),
                                )
                                ->leftjoin('employees AS supervisors', 'supervisors.id', '=', 'assigned_details.supervisor_id')
                                ->leftjoin('employees AS operators', 'operators.id', '=', 'assigned_details.operator_id')
                                ->leftjoin('machines', 'machines.id', '=', 'assigned_details.machine_id')
                                ->get();

        return $query;
    }
}
