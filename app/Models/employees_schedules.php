<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class employees_schedules extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'employee_id', 'schedule_id'
    ];
}
