<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedDetail extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'operator_id', 'supervisor_id', 'machine_id', 'urdimbre', 'meters', 'final_meters_prod', 'eficienty', 'operation_time', 'out_for_lunch'];


    public function operator(){
        return $this->hasOne('App\Models\Employee', 'id', 'operator_id');
    }

    public function supervisor(){
        return $this->hasOne('App\Models\Employee', 'id', 'supervisor_id');
    }

    public function machine(){
        return $this->hasOne('App\Models\Machine', 'id', 'machine_id');
    }
}
