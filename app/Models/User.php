<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\User;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    //protected $appends = ['employee_name', 'role_name'];

    /**
     * Get the employee name.
     *
     * @return bool
     */
    public function getEmployeeNameAttribute()
    {
        return $this->employee['first_name'] . ' ' . $this->employee['last_name'];
    }

    /**
     * Get the employee name.
     *
     * @return bool
     */
    public function getRoleNameAttribute()
    {
        return $this->role['name'];
    }



    public function scopeListado($query){
        return $query = $query->select('users.id',
                                'users.username',
                                'employees.first_name',
                                'roles.name as role')
                                ->leftJoin('employees', 'users.employee_id', '=', 'employees.id')
                                ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                                ->get();
    }

  /*  public function employee()
    {
        return $this->hasOne('App\Models\Employee')->select('first_name');
    }*/
    // public function employee(){
    //     return $this->belongsTo('App\Models\Employee')->select(array('id', 'first_name', 'last_name'));;
    // }

    public function employee() {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
    }

    public function role() {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }
}
