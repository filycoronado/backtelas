<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class TeamEmployee extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teams_employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id', 'employee_id', 'active'
    ];

    public function employee() {
        return $this->hasOne('App\Models\Employee', 'id', 'employee_id');
     }

     public function Team() {
         return $this->hasOne('App\Models\Team');
     }

}
