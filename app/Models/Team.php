<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function employees()
    {
        return $this->belongsToMany('App\Models\Employee', 'teams_employees', 'team_id', 'employee_id')
                    ->withPivot('active')
                    ->where('active', '=', 1)
                    ->whereNull('teams_employees.deleted_at')
                    ->withTimestamps();
    }

    public function teamEmployees()
    {
        return $this->belongsToMany('App\Models\TeamEmployee', 'teams_employees', 'team_id')->withPivot('active');
    }
}
