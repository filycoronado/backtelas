<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Machine extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pics', 'trama', 'name'
    ];

    public static function scopeListado($query, $start, $perPage, $search="")
    {
        $total = $query->count(); // count total rows
        if( $search != "")
        {
            $query->where('machines.pics', 'like', '%'. $search .'%')->orWhere('machines.trama', 'like', '%'. $search .'%');
        }
        $count = $query->count(); // count total rows

        $page=0;
        if( $perPage != 0 ){
            $query = $query->skip(intval($start-1))->take($perPage); //$query->skip(($start-1)*$perPage)->take($perPage);
            $page = (($start-1)+$perPage)/$perPage; //($start-1)+$perPage/$perPage;
        }

        $pages = round($count/$perPage);

        $query = $query->select('machines.id',
                                'machines.pics',
                                'machines.trama',)
                                ->get();

        $meta['recordsTotal'] = $total;
        $meta['recordsFiltered'] = $count;
        $meta['start'] = $start;
        $meta['page'] = round($page);
        $meta['perpage'] = $perPage;
        $meta['pages'] = round($pages);
        $meta['length'] = $count;

        $results = [
            'meta' => $meta,
            'recordsTotal' => $total,
            'recordsFiltered' => $count,
            'list' => $query
        ];
        return $results;
    }
}
