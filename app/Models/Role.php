<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    /* Lista de roles por indice */
    public static $list = array(
        1 => "super_admin",
        2 => "admin",
        3 => "supervisor"
    );

    /* definiciones de roles */
    public static $defList = array(
        "super_admin"   => 1,
        "admin"         => 2,
        "supervisor"    => 3
    );

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function getuser()
    {
        return $this->hasOne('App\Models\User');
    }
}
