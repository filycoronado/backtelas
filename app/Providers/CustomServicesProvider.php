<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\services\IUserService;
use App\services\implementation\UserService;

class CustomServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Services
        $this->app->bind('app\services\IUserService',
                         'app\services\implementation\UserService');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
