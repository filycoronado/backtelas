<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Classes\Response;
use App\Classes\Permissions;

use App\Models\Role;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $action, $object)
    {
        //dd($object);
        // Obtiene el usuario logueado
        $user = auth()->user();
        // Obtiene el rol que tiene el usuario
        $role = Role::find($user->role_id);
        // Obtiene la lista de permisos para dicho rol
        $permissions = json_decode($role->permissions, true);
        //var_dump($permissions[$object]);
        // Verifica si el rol tiene permisos para realizar la accion para el objeto
        if (!in_array($action, $permissions[$object])) {
            // Regresa un mensaje que no esta autorizado
            return Response::unauthorized(__('messages.unauthorized'));
        }
        // Verifica si el rol del usuario esta en la lista de permisos para la accion del objeto
        /* if (!in_array(Role::$list[$user->role_id], Permissions::$list[$object][$action])) {
            // Regresa un mensaje que no esta autorizado
            return Response::unauthorized(__('messages.unauthorized'));
        } */
        // Continua con la accion
        return $next($request);
    }
}
