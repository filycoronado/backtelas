<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\employees_schedules;
use App\Models\Employee;
use App\Classes\Response;

class ScheduleController extends Controller
{
   public function getAll(){
       return Schedule::all();
   }

   public function createSchedule(Request $request){
    $schedule=new Schedule();
    $schedule->fill($request->all());
    
    if($schedule->save()){
        return Response::success(__('messages.saved', ['name' => '']), $schedule);
    }else{
        return Response::badRequest(__('messages.save_failed', []), null);
    }
   }

   public function findById($id){
     return Schedule::find($id);
   }
   public function uppdateSchedule(Request $request){
    $schedule=new Schedule();
    $id=$request['id'];
    $schedule->fill($request->all());

    $oldSchedule=Schedule::find($id);
    $oldSchedule->name=$schedule->name;
    $oldSchedule->type=$schedule->type;
     if($oldSchedule->update()){
        return Response::success(__('messages.saved', ['name' => '']), $oldSchedule);
  }else{
    return Response::badRequest(__('messages.save_failed', []), null);
  }
   }
   public function getByEmployee($id){
    //$schedule=new Schedule();
    
    return  employees_schedules::find()->where("employee_id=".$id)->get();
   }


   public function saveEmployeeSchedule(Request $request){
     $emp=new employees_schedules();
     $emp->fill($request->all());
     $actualweek=$this->getWeek(date("Y-m-d"));

if(isset($request['id'])){

    $idr=$request['id'];// este es el que quiero asignar

    $item= employees_schedules::where('employee_id',$emp->employee_id)->first();// este es el asignado
    if($item==null){
      if($emp->save()){
        return Response::success(__('messages.saved', []), "Horario asignado con éxito");
      }else{
        return Response::success(__('messages.save_failed', []), null);
      }
    }
    $weekitem=$this->getWeek($item->created_at);
    $currentweek=$this->getWeek(date("Y-m-d"));
    if($weekitem==$currentweek){
      return Response::success(__('messages.save_failed', []), "Horario ya fue asignado esta semana");
    }else{
      //erase firt the last asigntaton
    $item->delete();
    if($emp->save()){
      return Response::success(__('messages.saved', []), "Horario asignado con éxito");
    }else{
      return Response::success(__('messages.save_failed', []), null);
    }

  }

}else{

  if($emp->save()){
    return Response::success(__('messages.saved', []), "Horario asignado con éxito");
  }else{
    return Response::success(__('messages.save_failed', []), "Error al crear horario1");
  }
}
return Response::success(__('messages.save_failed', []), "Error al crear horario2");
   }

   private function getWeek($date){

    $fecha=date("Y-m-d",strtotime($date)); 

    $dia   = substr($fecha,8,2);
    $mes = substr($fecha,5,2);
    $anio = substr($fecha,0,4);  
    
    
    $semana = date('W',  mktime(0,0,0,$mes,$dia,$anio)); 
 
    
    return  $semana;
}
}