<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Role;
use App\Classes\Response;
use App\Classes\Permissions;

use App\services\IUserService;

use Validator;
use Auth;
use URL;

class UserController extends Controller
{

    /**
     * unauthorized user
     *
     * @return [string] message
     */
    public function unauthorized()
    {
        return Response::unauthorized(__('messages.unauthorized', []));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doLogin(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required',
        ],
        [
            'username.required' => __('validation.required', ['attribute' => 'username']),
            'password.required' => __('validation.required', ['attribute' => 'password']),
        ]);

        if (!$validator->passes()) {
            return Response::badRequest(__('messages.required_fields', []), $validator->errors()->all());
        }

        $credentials = request(['username', 'password']);
        //return $credentials;
        if (!$token = auth('user')->attempt($credentials)) {
            return Response::unauthorized(__('messages.bad_credentials', ['attribute' => '']));
        }
        $user = auth('user')->user();
        //return $user;
        $tokenResult = $user->createToken('User Access Token');
        $token = $tokenResult->token;
        //$token->save();
        $data = [
            'username' => $user->username,
            'role' => $user->role->name,
            'name' => $user->employee->first_name,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'permissions' => $this->getUserPermissions($user)
        ];
        return Response::success(__('messages.logged_in', ['name' => '']), $data);
    }

    // Funcion para armar la lista de permisos que tiene el usuario
    public function getUserPermissions($user){
        $permissions = [];
        foreach (Permissions::$list as $keyModel => $models) {
            $array = [$keyModel => []];
            foreach ($models as $keyAction => $actions) {
                if (in_array(Role::$list[$user->role_id], $actions)) {
                    array_push($array[$keyModel], $keyAction);
                }
            }
            // Si no quiere mandar el modelo del permiso vacio descomentar el if
            //if (!empty($array[$keyModel])) {
                array_push($permissions, $array);
            //}
        }
        return $permissions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return $this->$users->FindAll();
         return User::listado();
       // return Response::success(__('messages.found', ['attribute' => 'Users']), $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addUser(Request $request)
    {
        $user = new User();
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->employee_id = $request->employee_id;
        $user->role_id = $request->role_id;
        $user->save();
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return $user;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        /* $user = new User();
        $id = $request['id'];
        $user = User::find($id);
        $user->fill($request->all());

        $oldUser = User::find($id);

        $oldUser->username = $user->username;

        if ($oldUser->update()) {
            return Response::success(__('messages.saved', ['name' => '']), $oldUser);
        }else{
          return Response::badRequest(__('messages.save_failed', []), null);
        } */
        $user = User::find($request->id);
        $user->fill($request->all());
        $user->password =  Hash::make($user->password);

        if ($user->update()) {
            return Response::success(__('messages.saved', ['name' => '']), $user);
        }else{
          return Response::badRequest(__('messages.save_failed', []), null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return 'destroy';
    }

    public function getCustom(){

      return  User::find(1)
      ->with('role')
      ->with('employee')
      ->get();
    }




    public function getUsersDto() {
        return User::listado();

    }
}
