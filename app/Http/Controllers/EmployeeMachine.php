<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\employees_machines;
use App\Models\Machine;


class EmployeeMachine extends Controller
{
    //

    public function getAll() {
        return employees_machines::all();
    }

    public function add(Request $request) {
        $employeeMachines = $request->all();
        foreach($employeeMachines as $em) {
            $employeeM = new employees_machines();
            $employeeM->fill($em);
            $employeeM->save();
        }
        // if ($employeeMachine->save()) {
        //     return Response::success(__('messages.saved', ['name' => '']), $employeeMachine);
        // }else{
        //     return Response::badRequest(__('messages.save_failed', []), null);
        // }
    }

    public function update(Request $request) {
        $employeeMachines = $request->all();
        foreach($employeeMachines as $em) {
            $employeeM = new employees_machines();
            $employeeM->fill($em);
            $employeeM->update();
        }
    }

    public function getByEmployee($id) {
        $employeeMachines = employees_machines::all();
        $result = Array();
        foreach ($employeeMachines as $em) {
            if ($em['employee_id'] == $id) {
                $m = Machine::find($em['machine_id']);
                array_push($result, $m);
            }
        }
        return $result;
    }

}
