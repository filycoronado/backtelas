<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Classes\Response;
use App\Classes\Permissions;

use App\Models\Role;

class RolesController extends Controller
{
    public function getAll() {
        return Role::all();
    }

    // Funcion para obtener la lista de todos los permisos
    public function getAllPermissions(Request $request){
        return Permissions::$all;
        // $permissions = Permissions::$all;
        // return Response::success(__('messages.found', ['attribute' => 'Permisos']), $permissions);
    }

    // Funcion para actualizar la lista de permisos del rol
    public function getRolePermissions(Request $request){
        $role = Role::find($request->id);
        $permissions = json_decode($role->permissions);
        return Response::success(__('messages.found', ['attribute' => 'Permisos']), $permissions);
    }

    // Funcion para actualizar la lista de permisos del rol
    public function updateRolePermissions(Request $request){
        $role = Role::find($request->id);
        $role->permissions = json_encode($request->permissions);
         if($role->update()){
            return Response::success(__('messages.saved', ['name' => 'Rol']), $role);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    }

    /* public function setPermissions(Request $request){
        $role = Role::find($request->id);
        $role->permissions = json_encode($this->getRolePermissions($role->id));
         if($role->update()){
            return Response::success(__('messages.saved', ['name' => 'Rol']), $role);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    } */

    // Funcion para armar la lista de permisos que tiene el usuario
    /*  public function getRolePermissions($role){
        $permissions = [];
        foreach (Permissions::$list as $keyModel => $models) {
            $array = [$keyModel => []];
            foreach ($models as $keyAction => $actions) {
                if (in_array(Role::$list[$role], $actions)) {
                    array_push($array[$keyModel], $keyAction);
                }
            }
            // Si no quiere mandar el modelo del permiso vacio descomentar el if
            //if (!empty($array[$keyModel])) {
                array_push($permissions, $array);
            //}
        }
        return $permissions;
    } */
}
