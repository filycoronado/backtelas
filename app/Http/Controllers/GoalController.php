<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Goal;
use App\Classes\Response;
use App\Models\TeamGoal;


class GoalController extends Controller
{
    public function getAll() {
        return Goal::all();
    }

    public function getById($id) {
        return Goal::find($id);
    }

    public function getByTeam($id) {
        $teamsgoals = TeamGoal::all();
        foreach ($teamsgoals as $tg) {
            if ($tg['team_id'] == $id) {
                return $tg['getGoal'];
            }
        }
    }

    public function addGoal(Request $request) {
        $goal = new Goal();
        $goal->fill($request->all());
        $goal->date = date('Y-m-d');
        if($goal->save()){
            return Response::success(__('messages.saved', ['attribute' => '']), $goal);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    }
}
