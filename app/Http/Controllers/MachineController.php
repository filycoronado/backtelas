<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Machine;

use App\Classes\Response;

class MachineController extends Controller
{
    public function paginated(Request $request) {
        $perPage = $request->has('perPage') ? intval($request->perPage) :  0 ; //pagina actual
        $start = $request->start; //pagina actual
        $search = $request->search;
        $order = $request->has('order') ? $request->order[0]: 'desc';
        $machines = Machine::listado($start, $perPage, $search, $order);
        if (count($machines) == 0) {
            return Response::success(__('messages.no_records_found', ['attribute' => 'Maquinas']), NULL);
        }
        return Response::success(__('messages.found', ['attribute' => 'Maquinas']), $machines);
    }

    public function getAll() {
        return Machine::all();
    }

    public function findById($id){
        $machine = Machine::find($id);
        if ($machine->count() == 0) {
            return Response::success(__('messages.no_records_found', ['attribute' => 'Maquina']), NULL);
        }
        return Response::success(__('messages.found', ['attribute' => 'Maquina']), $machine);
    }

    public function createMachine(Request $request){
        $machine = new Machine();
        $machine->fill($request->all());

        if($machine->save()){
            return Response::success(__('messages.saved', ['attribute' => 'Maquina']), $machine);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    }

    public function updateMachine(Request $request){
        $machine = Machine::find($request->id);
        $machine->pics=$request->pics;
        $machine->name=$request->name;
        $machine->trama=$request->trama;
         if($machine->update()){
            return Response::success(__('messages.saved', ['name' => 'Maquina']), $machine);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    }

    public function deleteMachine(Request $request){
        $machine = Machine::find($request->id);
        if (!$machine) {
            return Response::notFound(__('messages.not_found', ['attribute' => 'Maquina']));
        }
        if ($machine->delete()) {
            return Response::success(__('messages.deleted', ['attribute' => 'Maquina']), $machine);
        } else {
            return Response::success(__('messages.delete_failed', ['attribute' => 'Maquina']), $machine, FALSE);
        }
    }
}
