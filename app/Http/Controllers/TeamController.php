<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Team;
use App\Models\TeamEmployee;

use App\Classes\Response;

class TeamController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addTeam(Request $request)
    {
        $team = new Team();
        $team->fill($request->all());
        $team->save();
        return $team;
        //return Response::success(__('messages.saved', ['name' => 'Equipo']), $team);
    }

    public function updateTeam(Request $request) {
        $id = $request['id'];
        $team = Team::find($id);
        $team->fill($request->all());
        $team->update();
        return $team;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addEmployee(Request $request)
    {
        $team = Team::find($request->team_id);
        $employee = Employee::find($request->employee_id);
        $team->employees()->attach($employee);
        return Response::success(__('messages.saved', ['attribute' => 'Equipo']), NULL);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeEmployee(Request $request)
    {
        $team = Team::find($request->team_id);
        if($team->employees()->updateExistingPivot($request->employee_id, ['active' => 0])){
            return Response::success(__('messages.saved', ['attribute' => 'Equipo']), $team);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function syncTeamsEmployees(Request $request)
    {
        $teamsEmployees = $request->all();
        foreach($teamsEmployees as $te){
            if (array_key_exists("id", $te)) {
                $teamEmployee = TeamEmployee::find($te["id"]);
                if ($teamEmployee) {
                    $teamEmployee->fill($te);
                    $teamEmployee->save();
                }
            } else {
                $teamEmployee = new TeamEmployee();
                $teamEmployee->fill($te);
                $teamEmployee->save();
            }
        }
        return Response::success(__('messages.saved', ['attribute' => 'Equipo']), NULL);
    }

    public function teamEmployees($id) {
        $teamEmployees = TeamEmployee::all();
        $result = Array();
        foreach ($teamEmployees as $te) {
            if ($te['team_id'] == $id){
            if ($te['active'] == true) {
                $data = [
                    'id' => $te['id'],
                    'employee' => $te['employee'],
                    'team_id' => $te['team_id'],
                    'active' => $te['active']
            ];
                array_push($result, $data);
            }
        }
        }
        return $result;
    }

    public function getAll() {
        return Team::all();
    }

    public function getByid($id) {
        return Team::find($id);
    }
}
