<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\AssignedDetail;

use App\Classes\Response;

class AssignedDetailController extends Controller
{
    public function getAssignedDetail() {
        $assignedDetails = AssignedDetail::with('operator', 'supervisor', 'machine')->get();
        return Response::success(__('messages.found', ['attribute' => 'Detalle']), $assignedDetails);
    }
}
