<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\AssignedDetail;
use App\Models\Report;

use App\Classes\Response;
use App\Classes\Formulas;

use Carbon\Carbon;

// Excel export
use App\Exports\ReportExport;
use App\Exports\ReportExportFromView;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function generateReport() {
        setlocale(LC_ALL, 'es_ES');
        $report = Report::listado(NULL,  0, 0, "", NULL, NULL);
        /* $reports = [];
        foreach ($assignedDetails as $ad) {
            $report = new Report();
            // $report = $ad;
            $report->date = Carbon::parse($ad->created_at)->format('Y-m-d');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $startOfWeek = Carbon::parse($ad->created_at)->startOfWeek()->formatLocalized('%A %d %B %Y');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $endOfWeek = Carbon::parse($ad->created_at)->endOfWeek()->formatLocalized('%A %d %B %Y');
            $report->period = $startOfWeek . ' - ' . $endOfWeek;
            $report->week = Carbon::parse($ad->created_at)->weekOfYear;
            array_push($reports, $report);
        } */
        $assignedDetails = $report['data'];
        foreach ($assignedDetails as $ad) {
            //return $assignedDetails;
            //$report = new Report();
            // $report = $ad;
            $ad->date = Carbon::parse($ad->created_at)->format('Y-m-d');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $startOfWeek = Carbon::parse($ad->created_at)->startOfWeek()->formatLocalized('%A %d %B %Y');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $endOfWeek = Carbon::parse($ad->created_at)->endOfWeek()->formatLocalized('%A %d %B %Y');
            $ad->period = $startOfWeek . ' - ' . $endOfWeek;
            $ad->week = Carbon::parse($ad->created_at)->weekOfYear;
            $ad->month = Carbon::parse($ad->created_at)->month;
            $ad->urdim_percentage = Formulas::urdimPercentage($ad->urdimbre, $ad->meters);
            $ad->mesh = Formulas::mesh($ad->trama);
            $ad->loom_output = Formulas::loomOutput($ad->pics, $ad->mesh);
            $ad->production_100 = Formulas::production100($ad->loom_output, $ad->operation_time);
            $ad->production_90 = Formulas::production90($ad->production_100);
            $ad->production_85 = Formulas::production85($ad->production_100);
            $ad->efi_percentage = Formulas::efiPercentage($ad->meters, $ad->production_100);
            $ad->production_bonus = Formulas::productionBonus($ad->meters, $ad->production_85);
            //array_push($reports, $report);
        }
        return Response::success(__('messages.found', ['attribute' => 'Detalle']), $report);
    }

    public function generateReport2() {
        setlocale(LC_ALL, 'es_ES');
        //setlocale(LC_TIME, 'es_ES.utf8');
        //Carbon::setLocale(LC_TIME, 'es_ES.utf8');
        $assignedDetails = AssignedDetail::with('operator', 'supervisor', 'machine')->get();
        $reports = [];
        foreach ($assignedDetails as $ad) {
            $report = new Report();
            // $report = $ad;
            $report->date = Carbon::parse($ad->created_at)->format('Y-m-d');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $startOfWeek = Carbon::parse($ad->created_at)->startOfWeek()->formatLocalized('%A %d %B %Y');
            //setlocale(LC_TIME, 'es_ES.utf8');
            $endOfWeek = Carbon::parse($ad->created_at)->endOfWeek()->formatLocalized('%A %d %B %Y');
            $report->period = $startOfWeek . ' - ' . $endOfWeek;
            $report->week = Carbon::parse($ad->created_at)->weekOfYear;
            array_push($reports, $report);
        }
        return Response::success(__('messages.found', ['attribute' => 'Detalle']), $reports);
    }

    public function getChartsData(Request $request){
        $data = Report::chartsData();
        return Response::success(__('messages.found', ['attribute' => 'Detalle']), $data);
    }

    public function importExportView(Request $request){
        $data = Report::chartsData();
        return view('report/export', ['data' => $data]);
    }

    public function exportReport(Request $request){
        return Excel::download(new ReportExportFromView(), 'Reporte.xlsx');
    }
}
