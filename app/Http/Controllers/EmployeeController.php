<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use App\Models\Employee;
use App\Classes\Response;
use App\Models\User;

use Auth;

class EmployeeController extends Controller
{

    public function __construct(){
        $this->middleware('auth:api');
    }

    public function getAll() {
        return Employee::all();
    }

    public function DoEmployees(Request $request) {
        $perPage = $request->has('perPage') ? intval($request->perPage) :  0 ; //pagina actual
        $start = $request->start; //pagina actual
        $search = $request->search;
        $order = $request->has('order') ? $request->order[0]: 'desc';
        $employees = Employee::listado($start, $perPage, $search, $order);
        if (count($employees) == 0) {
            return Response::success(__('messages.not_found', ['attribute' => 'Empleados']), NULL);
        }
        return Response::success(__('messages.found', ['attribute' => 'Empleados']), $employees);
    }

    public function addEmployee(Request $request){

        $employee=new Employee();
        $employee->fill($request->all());
        if($employee->save()){
            return Response::success(__('messages.saved', ['name' => '']), $employee);
        }else{
            return Response::badRequest(__('messages.save_failed', []), null);
        }

    }

    public function getById($id) {
       return $employee = Employee::find($id);
    }

    public function availables(Request $request) {
       $employees = Employee::doesntHave('getuser')->get();
       return $employees;
    }

    public function getAvailableSupervisor() {
        $query = User::all();
        $result = Array();
        foreach ($query as $q) {
            $r = $q['role_id'];
            $res = Role::find($r);
            if ($res['name'] == 'supervisor') {
                array_push($result, Employee::find($query['employee_id']));
            }
        }
        return $result;
    }


    public function uppdateEmployee(Request $request){

        $id=$request['id'];

        $employee=Employee::find($id);

        $employee->fill($request->all());

         if($employee->update()){
            return Response::success(__('messages.saved', ['name' => '']), $employee);
      }else{
        return Response::badRequest(__('messages.save_failed', []), null);
      }
    }

    public function getSchedule($id) {
        $employees = Employee::find($id);
        return Response::success(__('messages.found', ['attribute' => 'Horario']), $employees->schedule);
     }

    public function getSuperVisorTeam(Request $request){
        $user = Auth::user();
        $employees = $user->employee->supervisorTeam->employees;
        if ($employees->count() == 0) {
            return Response::success(__('messages.no_records_found', ['attribute' => 'Employees']), NULL);
        }
        return Response::success(__('messages.found', ['attribute' => 'Employees']), $employees);
        //return $user->employee->supervisorTeam->employees;
        //$supervisor = User::find($request->)
    }

    public function getMachines(Request $request){
        $employee = Employee::find($request->id);
        $machines = $employee->machines;
        if ($machines->count() == 0) {
            return Response::success(__('messages.no_records_found', ['attribute' => 'Maquinas']), NULL);
        }
        return Response::success(__('messages.found', ['attribute' => 'Maquinas']), $machines);
    }
}
