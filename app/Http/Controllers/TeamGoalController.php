<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeamGoalController extends Controller
{
    //
   public function getAll() {
       return TeamGoal::all();
   }

   public function addTeamGoal(Request $request) {
    $teamGoal = new TeamGoal();
    $teamGoal->fill($request->all());
    if($teamGoal->save()){
        return Response::success(__('messages.saved', ['attribute' => 'teamGoal']), $teamGoal);
    }else{
        return Response::badRequest(__('messages.save_failed', []), null);
    }
   }

   public function getById($id) {
       return TeamGoal::find($id);
   }
}
