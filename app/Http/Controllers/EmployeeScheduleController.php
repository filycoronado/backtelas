<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\employees_schedules;
use App\Classes\Response;

class Employees_SchedulesController extends Controller
{
   


   public function saveEmployeeSchedule(Request $request){
    $scheduleEmployee=new employees_schedules();
    $scheduleEmployee->fill($request->all());
    return $scheduleEmployee;
   }
}