<?php

namespace App\Exports;

use App\Models\Report;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Report::chartsData();
    }

    public function headings(): array
    {
        return [
            'id',
            'supervisor_id',
            'operator_id',
            'machine_id',
            'urdimbre',
            'meters',
            'created_at',
            'machine_name',
            'supervisor',
            'operator'
        ];
    }
}
