<?php

namespace App\Exports;

use App\Models\Report;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReportExportFromView implements FromView, ShouldAutoSize, WithTitle
{

    public function view(): View
    {
        return view('report/export', [
          'data' => Report::chartsData()
        ]);
    }

    public function title(): string
    {
        return 'Metros y Urdimbre';
    }
}