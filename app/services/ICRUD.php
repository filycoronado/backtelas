<?php

namespace App\services;

 interface ICRUD {

   public function Add($object);
   public function Update($object);
   public function Find($id);
   public function FindAll();
   public function Delete($id);
}
