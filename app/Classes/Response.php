<?php

namespace App\Classes;

class Response
{
    /**
     * Success function
     * 
     * @param [string] message
     * @param [string] data
     * @return [string] response
     */
    public static function success($message, $data, $success = TRUE){
        return response()->json(self::buildResponse($success, $message, $data), 200);
    }

    /**
     * Bad Request function
     * 
     * @param [string] message
     * @return [string] response
     */
    public static function badRequest($message, $data = NULL){
        return response()->json(self::buildResponse(FALSE, $message, $data), 400);
    }

    /**
     * Unauthorize function
     * 
     * @param [string] message
     * @return [string] response
     */
    public static function unauthorized($message){
        return response()->json(self::buildResponse(FALSE, $message, NULL), 401);
    }

    
    /**
     * Not found function
     * 
     * @param [string] message
     * @return [string] response
     */
    public static function notFound($message){
        return response()->json(self::buildResponse(FALSE, $message, NULL), 404);
    }

    /**
     * Internal Server Error function
     * 
     * @param [string] message
     * @return [string] response
     */
    public static function internalServerError($message){
        return response()->json(self::buildResponse(FALSE, $message, NULL), 500);
    }

    /**
     * Build Response function
     * 
     * @param [boolean] success
     * @param [string] message
     * @param [object] data
     * @return [object] array
     */
    private static function buildResponse($success, $message, $data){
        return [
            'success' => $success, 
            'message' => $message, 
            'data' => $data
        ];
    }
}
