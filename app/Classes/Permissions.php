<?php

namespace App\Classes;

class Permissions
{
     // Permissions
     public static $all = [
        'user' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
        'employee' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
        'schedule' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
        'machine' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
        'goal' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
        'team' => [
            'viewAll',
            'view',
            'create',
            'update',
            'delete',
        ],
    ];

    // Permissions old
    public static $list = [
        'user' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
        'employee' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
        'schedule' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
        'machine' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
        'goal' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
        'team' => [
            'viewAll' => [
                'super_admin',
                'admin'
            ],
            'view' => [
                'super_admin',
                'admin'
            ],
            'create' => [
                'super_admin',
                'admin'
            ],
            'update' => [
                'super_admin',
                'admin'
            ],
            'delete' => [
                'super_admin',
                'admin'
            ],
        ],
    ];

}
