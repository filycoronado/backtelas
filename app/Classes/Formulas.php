<?php

namespace App\Classes;

class Formulas
{

    /**
     * % EFI
     * 
     * @param [number] meters
     * @param [number] production_100
     * @return [number] calculation
     */
    public static function efiPercentage($meters, $production_100){
        return ($meters*100)/$production_100;
    }

    /**
     * % URDIM
     * 
     * @param [number] urdimbre
     * @param [number] meters
     * @return [number] calculation
     */
    public static function urdimPercentage($urdimbre, $meters){
       if ($urdimbre>0) {
           return $urdimbre/$meters*100;
       }
       return 0;
    }

    /**
     * MESH
     * 
     * @param [number] trama
     * @return [number] calculation
     */
    public static function mesh($trama){
        return ($trama*2.54)/10;
    }

    /**
     * loom output m/h
     * 
     * @param [number] trama
     * @return [number] calculation
     */
    public static function loomOutput($pics, $mesh){
        return ($pics*1.524)/$mesh;
    }

    /**
     * PRODUCCION 100 %
     * 
     * @param [number] loom_output
     * @param [number] operation_time
     * @return [number] calculation
     */
    public static function production100($loom_output, $operation_time){
        return $loom_output*$operation_time;
    }

    /**
     * PRODUCCION 90 %
     * 
     * @param [number] production_100
     * @return [number] calculation
     */
    public static function production90($production_100){
        return $production_100*90/100;
    }

    /**
     * PRODUCCION 85 %
     * 
     * @param [number] production_100
     * @return [number] calculation
     */
    public static function production85($production_100){
        return $production_100*85/100;
    }

    /**
     * $ BONO PROD 
     * 
     * @param [number] production_100
     * @return [number] calculation
     */
    public static function productionBonus($meters, $production_85){
        if ($meters >= $production_85) {
            return (($meters - $production_85)/5*1.5);
        }
        return 0;
    }
}