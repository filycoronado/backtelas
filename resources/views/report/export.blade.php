<table>
    <thead>
        <tr>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Fecha</th>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Maquina</th>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Supervisor</th>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Operador</th>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Metros</th>
            <th style="background-color: #C6E0B4;text-align: center;font-size: 12px;border: 1px solid #afafaf;font-weight: bold;">Urdimbre</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $report)
        <tr style="text-align: center;">
            <td style="text-align: center;font-size: 12px;">{{$report->created_at}}</td>
            <td style="text-align: center;font-size: 12px;">{{$report->machine_name}}</td>
            <td style="text-align: center;font-size: 12px;">{{$report->supervisor}}</td>
            <td style="text-align: center;font-size: 12px;">{{$report->operator}}</td>
            <td style="text-align: center;font-size: 12px;border: 1px solid #afafaf;">{{$report->meters}}</td>
            <td style="text-align: center;font-size: 12px;border: 1px solid #afafaf;">{{$report->urdimbre}}</td>
        </tr>
        @endforeach
    </tbody> 
</table> 