<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Mensajes de idioma
	|--------------------------------------------------------------------------
	|
    | Las siguientes líneas de idioma contienen los mensajes predeterminados utilizados
    | en todo el sitio.
	|
    */

    'saved'             => 'Este :attribute ha sido guardado correctamente.',
    'saved_many'        => 'Todos los :attribute han sido guardados correctamente.',
    'deleted_many'      => 'Todos los :attribute han sido eliminados correctamente.',
    'deleted_from'      => 'Este :attribute ha sido borrado de :from.',
    'assign'            => 'Se asignaron los :attribute a :to.',
    'unassign'          => 'Se desasignaron los :attribute de :from.',
    'deleted_many'      => 'Todos los :attribute han sido borrados correctamente.',
    'try_again'         => 'Ha ocurrido un error, intentelo denuevo mas tarde.',
    'cannot_add_more'   => 'No se pueden agregar mas de :number :attribute a :to.',
    'exceed'            => 'El numero de :element excede :attribute.',
    'not_exist'         => 'El :attribute no existe.',
    'activated'         => 'El :attribute ha sido activado exitosamente.',
    'not_activated'     => 'No tiene ningun :attribute activado, favor de seleccionar uno.',
    'logged_in'         => 'Welcome :name.',
    'logged_out'        => 'Goodbye :name.',
    'no_records_found'  => 'No records found.',
    'found'             => ':attribute found.',
    'not_found'         => ':attribute not found.',
    'invalid_image'     => 'Imagen invalida.',
    'error_save'        => 'Error al guardar :attribute.',
    'error_db'          => 'Error al guardar en la base de datos.',
    'unauthorized'      => 'Unauthorized.',
    'vigency'           => 'Solo puedes ver información ya que faltan muy pocos días para tu evento.',
    'cannot_delete'     => 'No puede eliminar :adjective :attribute.',
    'published'         => 'Se ha publicado exitosamente el :attribute.',
    'rejected'          => 'Se ha rechazado el cambio de este :attribute.',
    'approved'          => 'Se ha aprobado el cambio.',
    'request_approval'  => 'Se ha solicitado una aprobación.',
    'cancel_request'    => 'Se ha cancelado la solicitud.',
    'not_payment'       => 'La aplicación ha sido bloqueada debido a un retraso en las mensualidades, te invitamos a ponerte al día para continuar con la planificación de tu evento',
    'bad_credentials'   => 'Email or password incorrect.',
    'required_fields'   => 'Please enter the required fields.',
    'invalid_format'    => 'Invalid :attribute format.',

    /* ACTIONS */
    'created'           => ':attribute created.',
    'updated'           => ':attribute updated.',
    'deleted'           => ':attribute deleted.',
    'activated'         => ':attribute activated.',
    'deactivated'       => ':attribute deactivated.',
    'file_uploaded'     => ':attribute uploaded successfully.',
    'update'            => 'Editó :attribute',
    'rejected_log'      => 'Se ha rechazado el cambio del :attribute',
    'approved_log'      => 'Se ha aprobado el cambio del :attribute',

    /* ACTIONS FAILED */
    'create_failed'     => 'Create :attribute failed.',
    'update_failed'     => 'Update :attribute failed.',
    'delete_failed'     => 'Delete :attribute failed.',
    'activate_failed'   => 'Activate :attribute failed.',
    'deactivate_failed' => 'Deactivate :attribute failed.',
    'file_upload_failed'=> ':attribute upload failed.',

    /* CAMPOS DE LOS MODELOS */
    'name'              => 'Nombre',
    'description'       => 'Descripción',
    'date_start'        => 'Fecha de inicio',
    'date_end'          => 'Fecha de finalización',
    'salon'             => 'Salón',
    'coordination'      => 'Precio por coordinación',
    'min_guests'        => 'Minimo de invitados',
    'max_guests'        => 'Maximo de invitados',
    'num_persons'       => 'Numero de personas',
    'phone'             => 'Teléfono',
    'notes'             => 'Notas',
    'status'            => 'Estatus',
    'id_client'         => 'Invitado por',
    'id_table'          => 'Mesa',
    'id_user'           => 'Responsable',
    'capacity'          => 'Capacidad',
    'comment'           => 'Comentario',
    'amount'            => 'Cantidad',
    'type'              => 'Tipo',
    'voucher'           => 'Comprobante',
    'max_files'         => 'Máximo de imaganes',
    'min_files'         => 'Mínimo de imaganes',
    'id_idea'           => 'Categoría',
    'role'              => 'Rol',
    'address'           => 'Dirección',
    'id_project'        => 'Evento',
    'app_status'        => 'Acceso a la aplicación',
    'app_status_reason' => 'Razón del bloqueo a la aplicación',
    'password'          => 'Contraseña',
    'phase_catalog_id'  => 'Fase',
    'order'             => 'Orden',
    'title'             => 'Título',
    'responsable'       => 'Responsable',
    'active'            => 'Activa'
];
